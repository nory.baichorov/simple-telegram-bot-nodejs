const TelegramApi = require('node-telegram-bot-api');
const { retryOptions, gameOptions } = require("./options");
const telegramToken = '2143140778:AAH0nuyXRC3A2HGWpdEzqxwngjT-tP3aCkI';
const bot = new TelegramApi(telegramToken, { polling: true });
const chats = {};

const startGame = async (chatId) => {
    await bot.sendMessage(chatId, 'Сейчас я загадаю число от 0 до 9, а ты должен его отгадать');
    const randomNumber = Math.floor(Math.random() * 10);
    chats[chatId] = randomNumber;
    await bot.sendMessage(chatId, 'Можешь отгадывать', gameOptions);
}

bot.setMyCommands([
    { command: '/start', description: 'Начальное приветствие' },
    { command: '/info', description: 'Информация о пользователе' },
    { command: '/game', description: 'Сыграть в игру' }
]);

const start = () => {
    bot.on('message', async msg => {
        const greeting = 'https://tlgrm.ru/_/stickers/f80/4f2/f804f23c-2691-332d-92e2-78bff6b9d47e/12.webp';

        if (msg.text === '/start') {
            await bot.sendSticker(msg.chat.id, greeting);
            return bot.sendMessage(msg.chat.id, `Добро пожаловать! Меня зовут Нод, я - Бот. Моего создателя зовут Норий`);
        }
        if (msg.text === '/info') {
            return bot.sendMessage(msg.chat.id, `Тебя зовут ${msg.from.first_name}`);
        }
        if (msg.text === '/game') {
           return startGame(msg.chat.id);
        }
        return bot.sendMessage(msg.chat.id, 'Я тебя не понимаю!');
    });

    bot.on('callback_query', async msg => {
        const num = msg.data;
        const chatId = msg.message.chat.id;

        if (num === '/again') {
            return startGame(chatId);
        }
        if (num === chats[chatId]) {
            return await bot.sendMessage(chatId, 'Поздравляю, ты угадал(а) число!)', retryOptions);
        } else {
            return await bot.sendMessage(chatId, `К сожалению, ты не угадал(а), я загадывал число ${chats[chatId]}`, retryOptions)
        }
    });
};

start();